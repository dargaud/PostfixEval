///////////////////////////////////////////////////////////////////////////////
// MODULE	PostfixEval
// PURPOSE	This is an extremely simple math parser
//			- postfix notation,
//			- no optimization for repetitive calls (that would be a thing to add!),
//			- directly uses the variables from the rest of your software,
//			- static stack size (easy to extend or make dynamic),
//			- only 2 types (double and boolean) which allows for logical operations,
//			- timing capabilities
//			- proper syntax and domain error control
// NOTE		ANSI C, optional C99, with a few additions easy to remove
// EXAMPLE	See the test cases at the end of this file
// TODO		Generate a call tree on 1st pass to speed up runtime afterwards
///////////////////////////////////////////////////////////////////////////////

#include <iso646.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>

#ifdef _CVI_
	#include <toolbox.h>	// For infinity and NAN checks. There are other ways to do this so this can be removed for standard C
	#include <utility.h>	// Only for Library error runtime checks
#else
	#include "UtilitySubstCVI.h"
#endif

#include "PostfixEval.h"

#ifdef PF_USETIMERS
	#include <time.h>
	#define MAX_FT 16						// Number of times the 'overtime' operator can be used
	static time_t LastFalseTime[MAX_FT];	// Set to the current time if B is false. Can only be used a limited number of times for all formulas
	int PostfixEval_FT=0;					// Must be reset to 0 between one series of evaluation to the next if you use 'overtime'
#endif

//#ifdef __STDC_HOSTED__
#if _XOPEN_SOURCE >= 600 or _ISOC99_SOURCE or _POSIX_C_SOURCE >= 200112L
	#define C99
#endif


// Because of union complaints on some compilers
#pragma DisableUninitLocalsChecking

//static char* Pos=NULL;	// Also used by the error message

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Perform a math evaluation of a postfix formula
/// ARRAY	VarList
/// OUT		Evaluation, PositionError
/// HIPAR	Formula/String with the formula to use, tokens are separated by spaces. No size limit
/// HIPAR	VarList/Array of variables which can be used in the formula
/// HIPAR	NbVar/Number of variables
/// HIPAR	Evaluation/Debug string (pass NULL to skip). Must have a size at least 10 times the formula
/// HIPAR	PositionError/Position of the error in the formula (pass NULL to ignore), returned as NULL if no error
/// HIRET	Return the result. Check its type before using the value
///////////////////////////////////////////////////////////////////////////////
tPE_Val PostfixEval_Compute(const char* const Formula,
	const tPE_Variable VarList[], const int NbVar,
	char* Evaluation, char** PositionError) {

	tPE_Val Ret={ 0., FALSE, PE_NOFORMULA, VAL_ERROR };
	int Prev, i, StackTop=0;	// Top free element on the stack
	tPE_Val Stack[STACK_SIZE]={ {0., FALSE, PE_INVALID, VAL_ERROR} };
	static char *Copy=NULL;		// We keep it and grow it if necessary
	char* Pos=NULL, *F=(char*)Formula, *C;

	Prev = SetBreakOnLibraryErrors (0);

	if (Formula==NULL or Formula[0]=='\0') { Ret.Err=PE_NOFORMULA; goto Error; }

	if            (Copy==NULL or strlen(Copy)<strlen(Formula))
		if (NULL==(Copy = realloc      (Copy, strlen(Formula)+1))) {
			Ret.Err=PE_MEMOUT; goto Error;
		}
	C=Copy;
	while (*F) *C++=(char)toupper(*F++);	// strcpy and uppercase convert
	*C='\0';

	Ret.Err=PE_STACKEMPTY;
	if (Evaluation!=NULL) Evaluation[0]='\0';

	Pos=strtok (Copy, " ");
	while (Pos!=NULL) {
		errno=0;
		///////////////////////////////////////////////////////////////////////
		#define IF(St) if (strcmp(St, Pos)==0)

		#define Top 	Stack[StackTop-1]	// Element on top of the stack
		#define Second	Stack[StackTop-2]	// Element below the top of the stack
		#define Third	Stack[StackTop-3]	// Third element from the top of the stack

		// The expected data type was not found on the stack. Exit with 'wrong type' error
		#define WRONG_TYPE { Ret.Err=PE_WRONGTYPE; goto Error; }

		// Check there there is at least 1, 2 or 3 operands on the stack
		#define Check1	{ if (StackTop<1) { Ret.Err=PE_STACKUNDERFLOW; goto Error; } }
		#define Check2	{ if (StackTop<2) { Ret.Err=PE_STACKUNDERFLOW; goto Error; } }
		#define Check3	{ if (StackTop<3) { Ret.Err=PE_STACKUNDERFLOW; goto Error; } }

		// Check that there is at least 1 Bool, 2 Bool, 1 Double, etc, on the stack
		#define CheckB1 { Check1; if (Top.Type!=VAL_BOOL) WRONG_TYPE }
		#define CheckB2 { Check2; if (Top.Type!=VAL_BOOL or Second.Type!=VAL_BOOL) WRONG_TYPE }
		#define CheckD1 { Check1; if (Top.Type!=VAL_DOUBLE) WRONG_TYPE }
		#define CheckD2 { Check2; if (Top.Type!=VAL_DOUBLE or Second.Type!=VAL_DOUBLE) WRONG_TYPE }
		#define CheckD3 { Check3; if (Top.Type!=VAL_DOUBLE or Second.Type!=VAL_DOUBLE or Third.Type!=VAL_DOUBLE) WRONG_TYPE }
		#define CheckDA { Check1; int j; for (j=0; j<StackTop; j++) if (Stack[j].Type!=VAL_DOUBLE) WRONG_TYPE }
		#define CheckBDD {Check3; if (Top.Type!=VAL_DOUBLE or Second.Type!=VAL_DOUBLE or Third.Type!=VAL_BOOL  ) WRONG_TYPE }

		// Set the type of a result, check for error, display it and continue
		#define EVB 	{ Top.Type=VAL_BOOL;   if (Evaluation!=NULL) sprintf(Evaluation, "%s, %s:%d", Evaluation, Pos, Top.B); goto Next; }
		#define EVD 	{ Top.Type=VAL_DOUBLE; if (Evaluation!=NULL) sprintf(Evaluation, "%s, %s:%f", Evaluation, Pos, Top.D); \
							if (IsInfinity  (Top.D)) { Ret.Err=PE_INF; goto Error; }	\
							if (IsNotANumber(Top.D)) { Ret.Err=PE_NAN; goto Error; }	\
							goto Next; \
						}

		///////////////////////////////////////////////////////////////////////
		// Start of the operator detection
		// If you add operators, put them also in the LIST_* macros

		// Boolean unary from boolean
		#define LIST_BuB "not !"
		IF("NOT")	{ CheckB1; Top.B = (Top.B?FALSE:TRUE); EVB; }	// I have no idea why, but doing Top.B=!Top.B fails when given 1
		IF("!")		{ CheckB1; Top.B = (Top.B?FALSE:TRUE); EVB; }	// Same

		// Boolean binary from booleans. See "iso646.h". I prefer them to the unintuitive C operators ! && || ^
		#define LIST_BbBB "and & or | xor eqv neqv nand nor"
		IF("AND")	{ CheckB2; Second.B= (Second.B and Top.B); StackTop--; EVB; }
		IF("&")		{ CheckB2; Second.B= (Second.B and Top.B); StackTop--; EVB; }
		IF("OR")	{ CheckB2; Second.B= (Second.B or  Top.B); StackTop--; EVB; }
		IF("|")		{ CheckB2; Second.B= (Second.B or  Top.B); StackTop--; EVB; }
		IF("XOR")	{ CheckB2; Second.B= (Second.B xor Top.B); StackTop--; EVB; }
		IF("EQV")	{ CheckB2; Second.B= (Second.B ==  Top.B); StackTop--; EVB; }
		IF("NEQV")	{ CheckB2; Second.B= (Second.B !=  Top.B); StackTop--; EVB; }
		IF("NAND")	{ CheckB2; Second.B=!(Second.B and Top.B); StackTop--; EVB; }
		IF("NOR")	{ CheckB2; Second.B=!(Second.B or  Top.B); StackTop--; EVB; }

#ifdef PF_USETIMERS
		// Boolean binary from a bool and a double
		#define LIST_BbBD "overtime"
		IF("OVERTIME") {	// Can be used only MAX_FT times
			time_t Time = time(NULL);
			Check2; if (Top.Type!=VAL_DOUBLE or Second.Type!=VAL_BOOL) WRONG_TYPE;
			if (Second.B==FALSE) LastFalseTime[PostfixEval_FT]=Time;	// !Second.B yields unexpected results !!!
			Second.B=(Second.B and difftime(Time, LastFalseTime[PostfixEval_FT])>=Top.D);
			PostfixEval_FT++;
			if (PostfixEval_FT>=MAX_FT) { Ret.Err=PE_TOOMANYTIMERS; goto Error; }
			StackTop--; EVB;
		}
#endif

		// Boolean binary from doubles
		#define LIST_BbDD "< > <= >= == !="
		/* This is imprecise due to rounding
		IF("<")		{ CheckD2; Second.B=(Second.D <  Top.D); StackTop--; EVB; }
		IF(">")		{ CheckD2; Second.B=(Second.D >  Top.D); StackTop--; EVB; }
		IF("<=")	{ CheckD2; Second.B=(Second.D <= Top.D); StackTop--; EVB; }
		IF(">=")	{ CheckD2; Second.B=(Second.D >= Top.D); StackTop--; EVB; }
		IF("=")		{ CheckD2; Second.B=(Second.D == Top.D); StackTop--; EVB; }
		IF("==")	{ CheckD2; Second.B=(Second.D == Top.D); StackTop--; EVB; }
		IF("!=")	{ CheckD2; Second.B=(Second.D != Top.D); StackTop--; EVB; }
		IF("<>")	{ CheckD2; Second.B=(Second.D != Top.D); StackTop--; EVB; }	*/

		IF("<")		{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) <  0; StackTop--; EVB; }
		IF(">")		{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) >  0; StackTop--; EVB; }
		IF("<=")	{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) <= 0; StackTop--; EVB; }
		IF(">=")	{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) >= 0; StackTop--; EVB; }
		IF("=")		{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) == 0; StackTop--; EVB; }
		IF("==")	{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) == 0; StackTop--; EVB; }
		IF("!=")	{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) != 0; StackTop--; EVB; }
		IF("<>")	{ CheckD2; Second.B=FP_Compare(Second.D, Top.D) != 0; StackTop--; EVB; }

		#define CheckErr {   if (errno==EDOM)	{ Ret.Err=PE_EDOM;	goto Error; }	\
						else if (errno==ERANGE)	{ Ret.Err=PE_ERANGE;goto Error; }	\
						else if (errno!=0)		{ Ret.Err=PE_MATH;	goto Error; }	\
						EVD; }

		// Double unary operators from double. With C99 there are other operators available
#ifdef C99
		#define LIST_DuD "-- ++ sign abs ceil floor frac inv, sin cos tan asin acos atan, sinh cosh tanh, ln log10 exp sqr sqrt, "\
						"nearbyint round exp2 expm1 log1p log2 cbrt erf erfc lgamma tgamma, "\
						"RadToDeg DegToRad CToK KToC"
#else
		#define LIST_DuD "-- ++ sign abs ceil floor frac inv, sin cos tan asin acos atan, sinh cosh tanh, ln log10 exp sqr sqrt, "\
						"RadToDeg DegToRad CToK KToC"
#endif
		IF("--")	{ CheckD1; Top.D--;              CheckErr; }
		IF("++")	{ CheckD1; Top.D++;              CheckErr; }
		IF("SIGN")	{ CheckD1; Top.D=Top.D>0?1:Top.D<0?-1:0; CheckErr; }
		IF("ABS")	{ CheckD1; Top.D= fabs	(Top.D); CheckErr; }
		IF("CEIL")	{ CheckD1; Top.D= ceil	(Top.D); CheckErr; }
#ifdef C99
		IF("NEARBYINT")	{ CheckD1; Top.D= nearbyint(Top.D); CheckErr; }
		IF("ROUND")	{ CheckD1; Top.D= round	(Top.D); CheckErr; }
#endif
		IF("FLOOR")	{ CheckD1; Top.D= floor	(Top.D); CheckErr; }
		IF("FRAC")	{ CheckD1; Top.D-=floor (Top.D); CheckErr; }	// Warning if negative
		IF("INV")	{ CheckD1; Top.D=     1./Top.D;  CheckErr; }

		IF("SIN")	{ CheckD1; Top.D= sin	(Top.D); CheckErr; }
		IF("COS")	{ CheckD1; Top.D= cos	(Top.D); CheckErr; }
		IF("TAN")	{ CheckD1; Top.D= tan	(Top.D); CheckErr; }
		IF("ASIN")	{ CheckD1; Top.D= asin	(Top.D); CheckErr; }
		IF("ACOS")	{ CheckD1; Top.D= acos	(Top.D); CheckErr; }
		IF("ATAN")	{ CheckD1; Top.D= atan	(Top.D); CheckErr; }

		IF("SINH")	{ CheckD1; Top.D= sinh	(Top.D); CheckErr; }
		IF("COSH")	{ CheckD1; Top.D= cosh	(Top.D); CheckErr; }
		IF("TANH")	{ CheckD1; Top.D= tanh	(Top.D); CheckErr; }

		IF("LN")	{ CheckD1; Top.D= log	(Top.D); CheckErr; }
		IF("LOG10")	{ CheckD1; Top.D= log10	(Top.D); CheckErr; }
		IF("EXP")	{ CheckD1; Top.D= exp	(Top.D); CheckErr; }
#ifdef C99
		IF("AXP2")	{ CheckD1; Top.D= exp2	(Top.D); CheckErr; }
		IF("EXPM1")	{ CheckD1; Top.D= expm1	(Top.D); CheckErr; }
		IF("LOG1P")	{ CheckD1; Top.D= log1p	(Top.D); CheckErr; }
		IF("LOG2")	{ CheckD1; Top.D= log2	(Top.D); CheckErr; }
#endif
		IF("SQR")	{ CheckD1; Top.D*=       Top.D;  CheckErr; }
		IF("SQRT")	{ CheckD1; Top.D= sqrt	(Top.D); CheckErr; }
#ifdef C99
		IF("CBRT")	{ CheckD1; Top.D= cbrt	(Top.D); CheckErr; }

		IF("ERF")	{ CheckD1; Top.D= erf	(Top.D); CheckErr; }
		IF("ERFC")	{ CheckD1; Top.D= erfc	(Top.D); CheckErr; }
		IF("LGAMMA"){ CheckD1; Top.D= lgamma(Top.D); CheckErr; }
		IF("TGAMMA"){ CheckD1; Top.D= tgamma(Top.D); CheckErr; }
#endif

		// Physics unary conversions
		IF("RADTODEG")	{ CheckD1; Top.D=       RAD_TO_DEG(Top.D); CheckErr; }
		IF("DEGTORAD")	{ CheckD1; Top.D=       DEG_TO_RAD(Top.D); CheckErr; }
		IF("CTOK")		{ CheckD1; Top.D=CELSIUS_TO_KELVIN(Top.D); CheckErr; }
		IF("KTOC")		{ CheckD1; Top.D=KELVIN_TO_CELSIUS(Top.D); CheckErr; }

		// Double binary operators from doubles
#ifdef C99
		#define LIST_DbDD "+ - * / %% ^, atan2 hypot, max2 min2 fmod"\
						", fdim copysign nextafter"
#else
		#define LIST_DbDD "+ - * / %% ^, atan2 hypot, max2 min2 fmod"
#endif
		IF("+")		{ CheckD2; Second.D=     (Second.D  +  Top.D); 			StackTop--; EVD; }
		IF("-")		{ CheckD2; Second.D=     (Second.D  -  Top.D); 			StackTop--; EVD; }
		IF("*")		{ CheckD2; Second.D=     (Second.D  *  Top.D); 			StackTop--; EVD; }
		IF("/")		{ CheckD2; Second.D=     (Second.D  /  Top.D); 			StackTop--; EVD; }
		IF("%")	    { CheckD2; Second.D=Top.D==0?0:100.*(Second.D/Top.D);   StackTop--; EVD; }	// We accept 0 0 % as 0
		IF("^")		{ CheckD2; Second.D=pow  (Second.D,    Top.D);			StackTop--; CheckErr; }
#ifdef C99
		IF("FDIM")	{ CheckD2; Second.D=fdim (Second.D,    Top.D);			StackTop--; CheckErr; }
		IF("COPYSIGN"){CheckD2;Second.D=copysign(Second.D, Top.D);			StackTop--; CheckErr; }
		IF("NEXTAFTER"){CheckD2;Second.D=nextafter(Second.D, Top.D);		StackTop--; CheckErr; }
#endif
		IF("ATAN2")	{ CheckD2; Second.D=atan2(Second.D,    Top.D);			StackTop--; CheckErr; }
		IF("MAX2")	{ CheckD2; Second.D=MAX  (Second.D,    Top.D);			StackTop--; CheckErr; }
		IF("MIN2")	{ CheckD2; Second.D=MIN  (Second.D,    Top.D);			StackTop--; CheckErr; }
		IF("HYPOT")	{ CheckD2; Second.D=sqrt (Second.D*Second.D+Top.D*Top.D);StackTop--;CheckErr; }
		IF("FMOD")	{ CheckD2; Second.D=fmod (Second.D,    Top.D);			StackTop--; CheckErr; }

		// Double ternary operators from 1 bool and 2 of either type
		#define LIST_DtBDD "?:"
		IF("?:")  { Check3; if (Third.Type!=VAL_BOOL) WRONG_TYPE
			if (Third.B) switch (   Third.Type=Second.Type) {
					case VAL_BOOL:  Third.B=   Second.B; break;
					case VAL_DOUBLE:Third.D=   Second.D; break;
			} else switch (         Third.Type=Top.Type) {
					case VAL_BOOL:  Third.B=   Top.B;    break;
					case VAL_DOUBLE:Third.D=   Top.D;    break;
			}
			StackTop-=2; EVD;
		}

		// Double multiple operators from the entire stack
		#define LIST_Dm "sum avg prod, max min maxindex minindex"
		IF("SUM")	{ CheckDA; for (i=1; i<StackTop; i++) Stack[0].D +=  Stack[i].D;                         StackTop=1; EVD; }
		IF("PROD")	{ CheckDA; for (i=1; i<StackTop; i++) Stack[0].D *=  Stack[i].D;                         StackTop=1; EVD; }
		IF("AVG")	{ CheckDA; for (i=1; i<StackTop; i++) Stack[0].D +=  Stack[i].D; Stack[0].D/=StackTop;   StackTop=1; EVD; }
		IF("MAX")	{ CheckDA; for (i=1; i<StackTop; i++) if (Stack[0].D<Stack[i].D) Stack[0].D =Stack[i].D; StackTop=1; EVD; }
		IF("MIN")	{ CheckDA; for (i=1; i<StackTop; i++) if (Stack[0].D>Stack[i].D) Stack[0].D =Stack[i].D; StackTop=1; EVD; }
		IF("MAXINDEX")  { int idx=0; CheckDA; for (i=1; i<StackTop; i++) if (Stack[0].D<Stack[i].D) { idx=i; Stack[0].D =Stack[i].D; } Stack[0].D =idx; StackTop=1; EVD; }
		IF("MININDEX")  { int idx=0; CheckDA; for (i=1; i<StackTop; i++) if (Stack[0].D>Stack[i].D) { idx=i; Stack[0].D =Stack[i].D; } Stack[0].D =idx; StackTop=1; EVD; }

		// Bool multiple operators from the entire stack
		#define LIST_Bm "within between"
		IF("WITHIN") {
			Check3; CheckDA; int j; BOOL B=TRUE;	// Check that A B C D Z all agree to abs([A..D]-[A..D])<Z
				for (i=0; i<StackTop-1; i++)
					for (j=i+1; j<StackTop-1; j++)
						B&=fabs(Stack[i].D-Stack[j].D)<Top.D; Stack[0].B=B; StackTop=1; EVB;
		}
		IF("BETWEEN") {
			Check3; CheckDA; BOOL B=TRUE;	// Check that A B C D m M all agree to m<=[A..D]<=M
				for (i=0; i<StackTop-2; i++)
					B&=(Stack[i].D<Second.D or Stack[i].D>Top.D); Stack[0].B=B; StackTop=1; EVB;
		}

		// Also possible to add type-agnostic stack operators
		#define LIST_StackOp "drop switch dup #"
		IF("DROP")	{ Check1; StackTop--;                             goto Next; }
		IF("SWITCH"){ tPE_Val V; Check2; V=Second; Second=Top; Top=V; goto Next; }
		IF("DUP")	{ Check1; StackTop++; Top=Second;                 goto Next; }
		IF("#")		{ Pos=NULL; /* This is a comment, skip the rest */break; }


		///////////////////////////////////////////////////////////////////////
		// Start of the variable name detection

		#define CheckOver if (StackTop==STACK_SIZE) { Ret.Err=PE_STACKOVERFLOW; goto Error; }
		for (i=0; i<NbVar; i++) {
			if (VarList[i].Name==NULL or VarList[i].Name[0]=='\0') continue;
			IF(VarList[i].Name) {
				CheckOver;
				StackTop++;
				switch (VarList[i].Type) {
					case VAL_DOUBLE:Top.Type=VAL_DOUBLE; Top.D=*(double*)VarList[i].pVal; EVD;
					case VAL_BOOL:	Top.Type=VAL_BOOL;   Top.B=*(BOOL  *)VarList[i].pVal; EVB;
					default:		Ret.Err=PE_INVALIDTYPE; goto Error;
				}
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Start of the constant detection

		// If you add operators, put them also in the LIST_* macros
		#define LIST_BOOL "True False RandB"
		IF("FALSE")		{ CheckOver; StackTop++; Top.Type=VAL_BOOL;   Top.B=0;    				EVB; }
		IF("TRUE")		{ CheckOver; StackTop++; Top.Type=VAL_BOOL;   Top.B=1;    				EVB; }
		IF("RANDB")		{ CheckOver; StackTop++; Top.Type=VAL_BOOL;   Top.B=rand()&1;    		EVB; }

		#define LIST_DOUBLE "Pi Euler, Planck ElemCharge LightSpeed Grav Avogadro Rydberg MolarGas, Rand"
		IF("PI")		{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=PI;					EVD; }
		IF("EULER")		{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=EULER;				EVD; }

		IF("PLANCK")	{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=PLANCK_CONSTANT;	EVD; }
		IF("ELEMCHARGE"){ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=ELEMENTARY_CHARGE;	EVD; }
		IF("LIGHTSPEED"){ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=SPEED_OF_LIGHT;		EVD; }
		IF("GRAV")		{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=GRAVITATIONAL_CONSTANT; EVD; }
		IF("AVOGADRO")	{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=AVOGADRO_CONSTANT;	EVD; }
		IF("RYDBERG")	{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=RYDBERG_CONSTANT;	EVD; }
		IF("MOLARGAS")	{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=MOLAR_GAS_CONSTANT;	EVD; }
		// Yes, I know, rand is not a const but it acts as one...
		IF("RAND") 		{ CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=(double)(rand())/RAND_MAX; EVD; }

		// Manual value
		if (strspn(Pos, ".0123456789+-E")==strlen(Pos)) {
			CheckOver; StackTop++; Top.Type=VAL_DOUBLE; Top.D=atof(Pos); CheckErr;
		}

		// Everything else
		Ret.Err=PE_UNKNOWN; goto Error;

		///////////////////////////////////////////////////////////////////////
		Next:	Pos=strtok(NULL, " ");
	}
	switch (StackTop) {
		default:Ret.Err=PE_REMAIN;		break;
		case 0: Ret.Err=PE_STACKEMPTY;	break;
		case 1: SetBreakOnLibraryErrors(Prev);
				if (PositionError!=NULL) *PositionError=NULL;
				tPE_Val *Ev=&Stack[0];
				switch (Ev->Type) {	// Added after removal of the union in tPE_Val
					case VAL_BOOL:   Ev->D= Ev->B;      Ev->Err=PE_NOERROR; break;
					case VAL_DOUBLE: Ev->B=(Ev->D!=0.); Ev->Err=PE_NOERROR; break;
					default:         Ev->D= Ev->B=0;    break;
				}
				return *Ev;	// Valid computation returns here
	}

Error:
	if (PositionError!=NULL) *PositionError=Pos;
	SetBreakOnLibraryErrors(Prev);
	Ret.D=Ret.B=0;
	Ret.Type=VAL_ERROR;
	return Ret;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return an error message from the Val.Err value
/// HIPAR	Err/Error code as found in Val.Err when Val.Type is VAL_ERROR
/// HIPAR	Pos/The PositionError returned by the computation function, or NULL for a shorter message
/// HIRET	Error string
///////////////////////////////////////////////////////////////////////////////
char* PostfixEval_ErrMsg_Wrapper(char* Str, const int Err, const char* Pos) {
//	static char Str[80]="";
	if (Pos==NULL)
		switch (Err) {
			default:
			case PE_NOERROR:		return "No error";
			case PE_NOFORMULA:		return "No formula";
			case PE_MEMOUT:			return "Out of memory";
			case PE_PARSING:		return "Parsing error (invalid formula)";
			case PE_MISSINGVAR:		return "Missing variable";
			case PE_MATH:			return "Math error";							// Cannot happen ?
			case PE_INF:			return "Infinity reached during computation";	// Happens with 1e300*1e300
			case PE_NAN:			return "Result is not a number (NaN)";
			case PE_EDOM:			return "Operand not in domain accepted by operator";
			case PE_ERANGE:			return "Result out of range";
			case PE_WRONGTYPE:		return "Wrong operand(s) type for this operator";
			case PE_STACKEMPTY:		return "Empty stack";
			case PE_STACKOVERFLOW:	sprintf(Str, "Stack overflow (only %d elements allowed)", STACK_SIZE); return Str;
			case PE_STACKUNDERFLOW:	return "Stack underflow (not enough operands available for this operator)";
			case PE_REMAIN:			return "More than one element remaining on stack at end of computation";
			case PE_INVALID:		return "Invalid value";
			case PE_UNKNOWN:		return "Unknown token";
			case PE_INVALIDTYPE:	return "Invalid variable type";
	#ifdef PF_USETIMERS
			case PE_TOOMANYTIMERS:	return "Command 'overtime' used too many times";
	#endif
		}
	else switch (Err) {
			default:
			case PE_NOERROR:		return       "No error";
			case PE_NOFORMULA:		return 		 "No formula";
			case PE_MEMOUT:			return       "Out of memory";
			case PE_PARSING:		sprintf(Str, "Parsing error (invalid formula) at token %s", Pos); return Str;
			case PE_MISSINGVAR:		return 		 "Missing variable";
			case PE_MATH:			sprintf(Str, "Math error at token %s", Pos); return Str;
			case PE_INF:			sprintf(Str, "Infinity reached during computation at token %s", Pos); return Str;
			case PE_NAN:			sprintf(Str, "Result is not a number (NaN) at token %s", Pos); return Str;
			case PE_EDOM:			sprintf(Str, "Operand not in domain accepted by operator %s", Pos); return Str;
			case PE_ERANGE:			sprintf(Str, "Result out of range at operator %s", Pos); return Str;
			case PE_WRONGTYPE:		sprintf(Str, "Wrong operand(s) type for operator %s", Pos); return Str;
			case PE_STACKEMPTY:		sprintf(Str, "Empty stack at operator %s", Pos); return Str;
			case PE_STACKOVERFLOW:	sprintf(Str, "Stack overflow (only %d elements allowed) at token %s", STACK_SIZE, Pos); return Str;
			case PE_STACKUNDERFLOW:	sprintf(Str, "Stack underflow (not enough operands available for operator %s)", Pos); return Str;
			case PE_REMAIN:			return       "More than one element remaining on stack at end of computation";
			case PE_INVALID:		sprintf(Str, "Invalid value %s", Pos); return Str;
			case PE_UNKNOWN:		sprintf(Str, "Unknown token %s", Pos); return Str;
			case PE_INVALIDTYPE:	sprintf(Str, "Invalid variable type for %s", Pos); return Str;
	#ifdef PF_USETIMERS
			case PE_TOOMANYTIMERS:	sprintf(Str, "Command 'overtime' used too many times for %s", Pos); return Str;
	#endif
		}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Like strcmp but compare sequences of digits numerically
///////////////////////////////////////////////////////////////////////////////
static int strcmpbynum(const char *s1, const char *s2) {
	for (;;) {
		if (*s2 == '\0')		return *s1 != '\0';
		else if (*s1 == '\0')   return -1;
		else if (!(isdigit(*s1) && isdigit(*s2))) {
			if (*s1 != *s2)
				return (int)*s1 - (int)*s2;
			else
				(++s1, ++s2);
		} else {
			char *lim1, *lim2;
			unsigned long n1 = strtoul(s1, &lim1, 10);
			unsigned long n2 = strtoul(s2, &lim2, 10);
			if      (n1 > n2) return  1;
			else if (n1 < n2) return -1;
			s1 = lim1;
			s2 = lim2;
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Auxiliary function to use the above with qsort
///////////////////////////////////////////////////////////////////////////////
static int compare(const void *p1, const void *p2) {
  const char * const *ps1 = p1;
  const char * const *ps2 = p2;
  return strcmpbynum(*ps1, *ps2);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return a custom help message
/// ARRAY	VarList
/// HIPAR	VarList/Array of variables which can be used in the formula
/// HIPAR	NbVar/Number of variables
/// HIPAR	DispVal/Also display the current values of the variables (not implemented)
/// HIPAR	VarPerLine/Max number of variable names per line (0 for unlimited)
/// HIPAR	Extra/Optional extra text to display at the bottom
/// HIRET	Return a string with help on multiple lines
///////////////////////////////////////////////////////////////////////////////
char *PostfixEval_Help(const tPE_Variable VarList[], const int NbVar, /*const BOOL DispVal,*/
	const BOOL UseColors, const int VarPerLine, const char* Extra) {
	int i, VarCount;
	static char *Str=NULL;
	static int LastVar=-1;
	char VarCol[16]="", OpCol[16]="", ConstCol[16]="", NormCol[16]="";
	// This is approximate but should avoid most buffer overflow problems
	// unless you have gigantic variable names or %f returns 32 digits on your system
	// You can free() the result ONLY if you don't call this function ever again.
	if (LastVar<NbVar)	// Grow only
		if (NULL==(Str=realloc(Str, 3000+30*(LastVar=NbVar)+strlen(Extra)))) return "";

	if (UseColors) {
		sprintf(VarCol,  "\033fg%06lX", VAL_DK_RED);
		sprintf(OpCol,   "\033fg%06lX", VAL_DK_GREEN);
		sprintf(ConstCol,"\033fg%06lX", VAL_DK_BLUE);
		sprintf(NormCol, "\033fg%06lX", VAL_BLACK);
	}

	sprintf(Str,
		"The evaluation is postfix, stack-based (reverse polish notation)\n"
		"The grammar for a formula is: Token Token Token...\n"
		"Where 'Token' can be an Operator or an Operand\n"
		"Operands are either a Variable or a Constant (detected in that order, case insensitive)\n"
		"and can be of two types: floats or booleans, which just add their value to the stack.\n"
		"Operators can act on 1, 2 or 3 operands or the entire stack and place one value on the stack.\n"
		"For non-commutative operators, think of the order as left to right: 'A B /' is A/B, 'A B <' is A<B.\n"
		"There is no assignement to a variable (you cannot change values in the program).\n"
		"In most cases the _type_ of the operands is important,\n"
		"i.e. most operators require a certain type operand(s).\n"
		" \n"	// A space is necessary otherwise the \n\n is turned into a \n into the display box
		"List of operators:\n"
		"   Boolean unary operators returning a boolean: %s" LIST_BuB "\n"
		"   Boolean binary operators returning a boolean: %s" LIST_BbBB "\n"
		"   Float unary operators returning a float: %s" LIST_DuD "\n"
		"   Float binary operators returning a float: %s" LIST_DbDD "\n"
		"   Float binary operators returning a boolean: %s" LIST_BbDD "\n"
		"   Ternary operator from 1 bool and 2 any type returning any type: %s" LIST_DtBDD "\n"
		"   Operators acting on the entire stack returning a float: %s" LIST_Dm "\n"
		"   Operators acting on the entire stack returning a bool: %s" LIST_Bm "\n"
		"   Stack operations (type agnostic): %s" LIST_StackOp "\n"
#ifdef PF_USETIMERS
		"   Timing operation: %s" LIST_BbBD " (\"B 10 overtime\" will return TRUE 10 seconds after B becomes and stays TRUE, and FALSE otherwise)\n"
#endif
		" \n"
		"Available float variables:\n   %s",
		OpCol, OpCol, OpCol, OpCol, OpCol, OpCol, OpCol, OpCol, OpCol,
#ifdef PF_USETIMERS
		OpCol,
#endif
		VarCol);

	// TODO: It would be a good idea to use nested functions here, but currently unsupported by CLANG compiler
	#define INSERT(T) \
				for (i=VarCount=0; i<NbVar; i++) \
					if (VarList[i].Name!=NULL and VarList[i].Name[0]!='\0' and VarList[i].Type==(T))\
						ValList[VC++]=VarList[i].Name;\
				qsort(ValList, VC, sizeof(char*), compare);\
				for (i=VarCount=0; i<VC; i++) {\
					/*if (VarPerLine>0 and ++VarCount>VarPerLine) { sprintf(Str+strlen(Str), "\n   %s", VarCol); VarCount=0; }*/\
					/*else*/ if (i>0 and (ValList[i][0]!=ValList[i-1][0] or \
								ValList[i][0]=='G' and ValList[i-1][0]=='G' and atoi(&ValList[i][1])!=atoi(&ValList[i-1][1]))) \
						/*strcat(Str+strlen(Str), " -");*/\
						{ sprintf(Str+strlen(Str), "\n   %s", VarCol); VarCount=0; }\
					sprintf(Str+strlen(Str), " %s", ValList[i]);\
				}

	char *ValList[1000];	// 1000 variables of up to 80 char
	int VC=0;
	INSERT(VAL_DOUBLE);

	sprintf(Str+strlen(Str), " \nAvailable boolean variables:\n   %s", VarCol);
	VC=0;
	INSERT(VAL_BOOL);


	sprintf(Str+strlen(Str), " \n"
		"Warning, make sure not to use an operator or constant name as a variable name.\n"
		" \n"
		"Constants are tokenized last:\n"
		"   Boolean constants: %s" LIST_BOOL "\n"
		"   Float constants: %s-1.234e30 0 12 " LIST_DOUBLE "\n   %s(Inf and NaN are reserved keywords)\n"
		" \n"
		"At the end there should be exactly one bool or float value left on the stack: the result of the computation.\n"
		"An error code is returned otherwise.\n"
		" \n"
		"Examples:\n"
		"  A1 A2 > A3 A4 >= and    # return TRUE if A1>A2 and A3>=A4\n"
		"  A1 A2 A3 A4 min         # return the minimum value of the list\n"
		"  A1 A2 A3 A4 B within    # Verify that Max(An)-Min(An)<=B\n"
		"  A1 A2 A3 A4 N M between # Verify that all N<=An<=M\n"
		"  rand rand rand rand avg # return a random number with pseudo-gaussian distribution\n"
		"  D1 B2 switch drop       # return B2\n"
		"  LightSpeed sqr A *      # return A.C^2\n"
		"  Y X atan2 RagToDeg      # return atan(Y/X) in degree without error even if X=0\n"
		"  Y X hypot               # return sqrt(X^2+Y^2)\n"
		"  B R Q ?:                # return R if B is true, Q otherwise\n",
		ConstCol, ConstCol, NormCol);

	if (Extra!=NULL and strlen(Extra)>0)
		sprintf(Str+strlen(Str), " \n%s", Extra);

	sprintf(Str+strlen(Str), "\n \nDouble-clic here to send this text to stdout.\n ");

	return Str;
}

#ifdef TEST_POSTFIXEVAL
///////////////////////////////////////////////////////////////////////////////
/// HIFN	Only for testing the PostfixEval code
/// HIFN	Compile with: gcc -o TestPostfixEval -DTEST_POSTFIXEVAL PostfixEval.c -lm
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	double A1=12, D1=23, C1=1.234e5, C2=-20.5e-3, Beh=1;
	BOOL B1=TRUE, B2=FALSE;
	tPE_Variable VarList[10];
	tPE_Val Val;
	int NV=0;
	char Formula[255],
		*Pos=NULL,
		Evaluation[1024]; 	// returns an evaluation string useful for debugging the syntax
		//*Evaluation=NULL;		// Also try this

	VarList[NV].Name="A1";	VarList[NV].pVal=&A1;	VarList[NV++].Type=VAL_DOUBLE;
	VarList[NV].Name="D1";	VarList[NV].pVal=&D1;	VarList[NV++].Type=VAL_DOUBLE;
	VarList[NV].Name="B1";	VarList[NV].pVal=&B1;	VarList[NV++].Type=VAL_BOOL;
	VarList[NV].Name="B2";	VarList[NV].pVal=&B2;	VarList[NV++].Type=VAL_BOOL;
	VarList[NV].Name="C1";	VarList[NV].pVal=&C1;	VarList[NV++].Type=VAL_DOUBLE;
	VarList[NV].Name="C2";	VarList[NV].pVal=&C2;	VarList[NV++].Type=VAL_DOUBLE;
	VarList[NV].Name="13";	VarList[NV].pVal=&Beh;	VarList[NV++].Type=VAL_DOUBLE;
		// Yes, we give the value of 1 to the variable named '13' (not a good idea but it works) !!!

	// Display help
	puts(PostfixEval_Help(VarList, NV, FALSE, 10, ""));

	#define PE_TEST(String)	\
		strcpy(Formula, String);	\
		Val=PostfixEval_Compute(Formula, VarList, NV, Evaluation, &Pos /*also try NULL*/);	\
		switch (Val.Type) {	\
			case VAL_ERROR: printf("\n\"%s\" gave error \"%s\"\nExpected %s\nEvaluation=[%s]\n", 	\
									Formula, PostfixEval_ErrMsg(Val.Err, Pos), P, Evaluation==NULL?"N/A":Evaluation); break;	\
			case VAL_BOOL:  printf("\n\"%s\" is %s\nExpecting %s%s\nEvaluation=[%s]\n",	\
									Formula, Val.B?"TRUE":"FALSE", B?"TRUE":"FALSE", Val.B!=B?" DISCREPANCY!":"", Evaluation==NULL?"N/A":Evaluation);  break;	\
			case VAL_DOUBLE:printf("\n\"%s\" = %f\nExpecting %f%s\nEvaluation=[%s]\n",	\
									Formula, Val.D, R, Val.D!=R?" DISCREPANCY!":"", Evaluation==NULL?"N/A":Evaluation); break;	\
			default:		printf("\nError, invalid result type %d\nEvaluation=[%s]\n",	\
									Val.Type, Evaluation==NULL?"N/A":Evaluation); break;	\
		}

	// Expected results
	double R;
	BOOL B;
	char *P;

	P="unexpected";		// Testing valid computations
	R=0; B=!!!!FALSE; 					PE_TEST("FALSE not ! not not");	// Mixed syntax
	R=0; B=((B1 or B2) == (B1 and B2));	PE_TEST("B1 B2 or B1 B2 & eqv");
	R=(A1+10)/D1; B=0;					PE_TEST("A1 10 + D1 /");
	R=0; B=!(A1>5);						PE_TEST("A1 5 > not");
	R=0; B=C1>=exp(D1-A1);				PE_TEST("C1 e D1 A1 - ^ >=");
	R=sin(PI/4); B=0;					PE_TEST("C1 drop PI 4 / sin");
	R=1+A1+D1+2+3+4-50; R*=R; B=0; P="";PE_TEST("1 A1 D1 2 3 4 -50 sum 2 ^");
	R=0; B=0; P="";						PE_TEST("rand rand rand rand rand rand rand min");	// OK, won't be 0
	R=(B1?A1:C1); B=0; P="";			PE_TEST("B1 A1 C1 ?:");
	R=(A1+2+3+4+5)/5.; B=0; P="";		PE_TEST("A1 2 3 4 5 avg");
	R=0; B=B2; P="";					PE_TEST("D1 B2 switch drop");
	R=C2*SPEED_OF_LIGHT*SPEED_OF_LIGHT; B=0; P=""; PE_TEST("LightSpeed dup * C2 *");
#ifdef PF_USETIMERS
	R=0; B=0; P="";						PE_TEST("TRUE 3 overtime");// Discrepancy here is normal
	PostfixEval_FT=0;
	Delay(5);							// After 3 seconds the result turns to true
	R=0; B=1; P="";						PE_TEST("TRUE 3 overtime");
#endif

	// Valid but beware !
	R=12.34; B=0; P="";					PE_TEST("12.34.56e78");	// Syntax truncation. An error would be more apt
	R=Beh+1; B=0; P="";					PE_TEST("13 1 +");		// Careful with your variable names !

	R=B=0; 	// Testing for error conditions
	P="stack underflow";	PE_TEST("2 tan atan2");
	P="stack underflow";	PE_TEST("2 + 3");
	P="empty stack";		PE_TEST("2 3 + drop");
	P="unkown variable";	PE_TEST("A1 A2 + drop");
	P="remaining values";	PE_TEST("1 2 + 3 4 -");
	P="stack overflow";		PE_TEST("1 2 3 4 5 6 7 8 9 TRUE FALSE 1.2 -2.3 -1e5 A1 D1 C1 C2 10 11 PI E 13 14 15");
	P="unknown token";		PE_TEST("2 prout 4");
	P="math error";			PE_TEST("1 2 3 -1 sqrt");
	P="out of range";		PE_TEST("1.23e2000 sqrt");
	P="out of range";		PE_TEST("1.23e20 exp");
	P="wrong type";			PE_TEST("A1 C1 C2 == hypot");
	P="garbage syntax";		PE_TEST("A1,A2");
	P="extra invalid chars";PE_TEST("12,345");
	P="math overflow";		PE_TEST("9 8 7 6 5 4 3 2 ^ ^ ^ ^ ^ ^ ^");
	P="math overflow";		PE_TEST("1e300 dup * 1 switch /");

	return 0;
}
#endif

