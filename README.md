# PostfixEval

This is an extremely simple math parser which can act on internal C variables.
Its characteristics:
* postfix notation,
* no optimization for repetitive calls (that would be a thing to add!),
* directly uses the variables from the rest of your software,
* static stack size (easy to extend or make dynamic),
* only 2 types (double and boolean) which allows for logical operations,
* timing capabilities (changes state after a given time)
* proper syntax and domain error control

ANSI C, optional C99, with a few additions easy to remove

EXAMPLE: See the test cases at the end of this file
