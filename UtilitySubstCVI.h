#ifndef __UTILITY_SUBSTITUTE_CVI_H
#define __UTILITY_SUBSTITUTE_CVI_H

// This file provides some replacements for LabWindows/CVI functions and constants not found in other compilers

#include <unistd.h>	// just for sleep

#define SetBreakOnLibraryErrors(a) 0
#define FP_Compare(a, b) ((a)==(b) ? 0 : (a)>(b) ? 1 : -1)	// FIXME: this should be re-written to allow for epsilon
#define IsInfinity(a) (!isfinite(a))
#define IsNotANumber(a) isnan(a)
#define Delay(a) sleep(a)

// This is from utility.h
#define PI                          3.1415926535897932384626433832795028841971
#define TWO_PI                      6.28318530717958646
#define HALF_PI                     1.57079632679489661
#define THREE_HALVES_PI             4.71238898038468990
#define RECIPROCAL_OF_PI            0.31830988618379067153
#define LN_OF_PI                    1.14472988584940017414
#define LOG_10_OF_PI                0.49714987269413385435
#define EULER                       2.71828182845904523536
#define RECIPROCAL_OF_EULER         0.36787944117144232159
#define LOG_10_OF_EULER             0.4342944819032518276511289189166050822944
#define LN_OF_10                    2.3025850929940456840179914546843642076011
#define LN_OF_2                     0.6931471805599453094172321214581765680755

#define PLANCK_CONSTANT             6.626176e-34        /* 1/Hz, uncertainty 5.4 ppm */
#define ELEMENTARY_CHARGE           1.6021892e-19       /* Coulombs, uncertainty 2.9 ppm */
#define SPEED_OF_LIGHT              299792458.0         /* Meters/Second, uncertainty 0.004 ppm */
#define GRAVITATIONAL_CONSTANT      6.6720e-11          /* N*M^2/K^2, uncertainty 615 ppm */
#define AVOGADRO_CONSTANT           6.022045e23         /* 1/mol, uncertainty 5.1 ppm */
#define RYDBERG_CONSTANT            10973731.77         /* 1/m, uncertainty 0.075 ppm */
#define MOLAR_GAS_CONSTANT          8.31441             /* 1/(m * K), uncertainty 31 ppm */

#define RAD_TO_DEG(r)               ((r)*(360.0/TWO_PI))
#define DEG_TO_RAD(d)               ((d)*(TWO_PI/360.0))
#define CELSIUS_TO_KELVIN(c)        ((c) + 273.15)
#define KELVIN_TO_CELSIUS(k)        ((k) - 273.15)

// This is from userint.h
#define VAL_RED                         0xFF0000L /* 16 standard colors */
#define VAL_GREEN                       0x00FF00L
#define VAL_BLUE                        0x0000FFL
#define VAL_CYAN                        0x00FFFFL
#define VAL_MAGENTA                     0xFF00FFL
#define VAL_YELLOW                      0xFFFF00L
#define VAL_DK_RED                      0x800000L
#define VAL_DK_BLUE                     0x000080L
#define VAL_DK_GREEN                    0x008000L
#define VAL_DK_CYAN                     0x008080L
#define VAL_DK_MAGENTA                  0x800080L
#define VAL_DK_YELLOW                   0x808000L
#define VAL_LT_GRAY                     0xC0C0C0L
#define VAL_DK_GRAY                     0x808080L
#define VAL_BLACK                       0x000000L
#define VAL_WHITE                       0xFFFFFFL
#define VAL_GRAY                        0xA0A0A0L
#define VAL_OFFWHITE                    0xE0E0E0L
#define VAL_TRANSPARENT                 0x1000000L

#endif
