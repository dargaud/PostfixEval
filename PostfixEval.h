#ifndef __POSTFIX_EVAL_H
#define __POSTFIX_EVAL_H

// User setable macros

// Define this macro if you want to use the 'overtime' command for timed test
// (if TRUE for N seconds then ...)
// Comment this out if unwanted
#define PF_USETIMERS

// Using a bigger pile leads to slow results in case of runaway piling
#define STACK_SIZE 16




#include "Def.h"	// For BOOL, TRUE, FALSE

// Available types. This is more or less arbitrary
#ifndef VAL_DOUBLE
#define VAL_DOUBLE 4	// CVI compatibility - this is not the number of bytes
#endif
#define VAL_BOOL 12
#define VAL_ERROR (-1)


#ifdef PF_USETIMERS
	extern int PostfixEval_FT;	// Must be reset to 0 between one series of evaluation to the next if you use 'overtime'
#endif

// Error codes
// See PostfixEval_ErrMsg() for messages
enum {
	PE_NOERROR,
	PE_NOFORMULA,
	PE_MEMOUT,
	PE_PARSING,
	PE_MISSINGVAR,
	PE_WRONGTYPE,
	PE_MATH,
	PE_INF,
	PE_NAN,
	PE_EDOM,
	PE_ERANGE,
	PE_STACKEMPTY,
	PE_STACKUNDERFLOW,
	PE_STACKOVERFLOW,
	PE_REMAIN,
	PE_INVALID,
	PE_UNKNOWN,
	PE_INVALIDTYPE,
#ifdef PF_USETIMERS
	PE_TOOMANYTIMERS,
#endif
};


// Points to a real variable in the main program memory
typedef struct sPE_Variable {
	void *pVal;	// Pointer to the variable value
	char *Name;	// Name of the variable as used in the formulas (use something short)
	int Type;	// Indicates what the pointer points to: VAL_DOUBLE, VAL_BOOL or 0 if not in use
} tPE_Variable;

// Result of a computational evaluation:
// either a double, a bool or an error code
// I don't want to add integer arithmetics because it becomes hard to discriminate with doubles
typedef struct sPE_Val {
//	union {	// Removed the union, this way we can do D=B for consistency
		double D;	// For doubles
		BOOL   B;	// For booleans
		int  Err;	// For error codes
//	};
	int Type;		// Indicate what's in the union: VAL_DOUBLE, VAL_BOOL or VAL_ERROR
} tPE_Val;

extern tPE_Val PostfixEval_Compute(const char* const Formula,
	const tPE_Variable VarList[], const int NbVar,
	char* Evaluation, char** PositionError);

#define PostfixEval_ErrMsg(Err, Pos) PostfixEval_ErrMsg_Wrapper((char[80]) {0}, Err, Pos)
extern char* PostfixEval_ErrMsg_Wrapper(char*Str, const int Err, const char* Pos);

extern char *PostfixEval_Help(const tPE_Variable VarList[], const int NbVar,
				/*const BOOL DispVal,*/ const BOOL UseColors, const int VarPerLine, const char* Extra);

#endif
