TARGET=PostfixEval
GCC=gcc
COPT=

all:
	$(GCC) $(COPT) -c $(TARGET).c
	$(GCC) $(COPT) -o  Test_$(TARGET) $(TARGET).c -DTEST_POSTFIXEVAL -lm
